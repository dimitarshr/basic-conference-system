# README #

### What is this repository for? ###

The Conference System consists of a number of classes. The first one is the Attendee class. The class conforms the following specification:

* Attendee Reference Number
* First Name
* Second Name 
* InstitutionName
* Conference Name
* Registration Type
* Paid
* Presenter
* Paper Title 

It allows a Conference attendee to register and get an invoice based on the registration type. The project also demonstrates 
Inheritance *(the person class)* and Abstraction *(the institution class)*.

# For more information, please refer to the *Class Diagram and non-GUI classes* PDF file and the *source code*. #