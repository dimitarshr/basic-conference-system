﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The Attendee class consists of all the associated properties required to store the data for a person going on a conference. 
 *              An instance of this class is the equivalent of a person attending the Conference.
 *              It inherits all the properties from Person class and within it an instance of the Insitution class is created.
 * Date last modified: 23/10/2016 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _40201757___Conference_System
{
    // The attendee inherits all the properties from Person class.
    public class Attendee : Person
    {
        // These are all the private variables associated with the Attendee class.
        private int attendeeRef;
        private Institution academy = new Institution();
        private string conferenceName;
        private string registrationType;
        private bool paid;
        private bool presenter;
        private string paperTitle;

        // AttendeeRef property is used to get, set and validate the Reference number of the Attendee.
        public int AttendeeRef
        {
            set
            {
                // If the validation fails an exception is thrown.
                if (value < 40000 || value > 60000)
                {
                    throw new ArgumentException("The reference number must be between 40000 and 60000!");
                }
                // Otherwise the attendeeRef value is set.
                attendeeRef = value; 
            }
            // Returns the attendeeRef value.
            get { return attendeeRef; }
        }

        // AcademyName property is used to set and get the name of the Insitution from the reference to the instance object academy which is of type Institution.
        public string AcademyName
        {
            // The Institution name value is set.
            set { academy.InstitutionName = value; }
            // Returns the name of the Insitution.
            get { return academy.InstitutionName; }
        }

        // AcademyAddress property is used to set and get the address of the Insitution from the reference to the instance object academy which is of type Institution.
        public string AcademyAddress
        {
            // The Institution address value is set.
            set { academy.InstitutionAddress = value; }
            // Returns the address of the Insitution.
            get { return academy.InstitutionAddress; }
        }

        // ConferenceName property is used to get, set and validate the name of the Conference.
        public string ConferenceName
        {
            set
            {   // If the validation fails an exception is thrown.
                if (value.Trim() == "")
                {
                    throw new ArgumentException("All the fields with * must be populated!");
                }
                // Otherwise the conferenceName value is set.
                conferenceName = value.Trim();
            }
            // Returns the conferenceName value.
            get { return conferenceName; }
        }

        // RegistrationType property is used to get, set and validate the registration type.
        public string RegistrationType
        {
            set
            {   // If the validation fails an exception is thrown.
                if (value != "Full" && value != "Student" && value != "Organiser")
                {
                    throw new ArgumentException("All the fields with * must be populated!");
                }
                // Otherwise the registrationType value is set.
                registrationType = value; 
            }
            // Returns the registrationType value.
            get { return registrationType; }
        }

        // Paid property is used to set and get the value of paid.
        public bool Paid
        {
            // Set the value to paid.
            set { paid = value; }
            // Returns the value of paid.
            get { return paid; }
        }

        // Presenter property is used to set and get the valud of presenter.
        public bool Presenter
        {
            // Set the value to presenter.
            set { presenter = value; }
            // Returns the value of presenter.
            get { return presenter; }
        }

        // PaperTitle property is used to get, set and validate the paper title value.
        public string PaperTitle
        {
            set
            {
                // In case the person is not a presenter the value of paperTitle is set to empty string.
                if (!presenter)
                {
                    paperTitle = "";
                }
                // In case the person is presenter and does not provide a paperTitle value an exception is thrown.
                if (presenter && value.Trim() == "")
                {
                    throw new ArgumentException("All the fields with * must be populated!");
                }
                // In case the person is presenter and a paperTitle is provided the value of paperTitle is set.
                if (presenter && value.Trim() != "")
                    paperTitle = value;
            }
            // Returns the value of paperTitle.
            get { return paperTitle; }
        }
        
        // The getCost() method returns the amount of money that is to be paid by the attendee.
        public double getCost()
        {
            string attendeeType = RegistrationType;
            double moneyToPay = 0;
            // Depending on the attendee type the money to be paid is different.
            switch (attendeeType)
            {
                case "Full": moneyToPay = 500; break;
                case "Student": moneyToPay = 300; break;
                case "Organiser": moneyToPay = 0; break;
            }
            // If the attendee is presenter, he/she gets a 10% discount.
            if (Presenter)
            {
                return moneyToPay - moneyToPay * 0.1;
            }
            // If not I just return the value without the discount.
            return moneyToPay;
        }
    }
}
