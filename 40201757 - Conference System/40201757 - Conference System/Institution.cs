﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The Institution class consists of all the associated properties needed for an institution. 
 *              An instance of this class is the equivalent of place where a conference takes place.
 *              An instance of the this class is created in the Attendee object.
 * Date last modified: 17/10/2016 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _40201757___Conference_System
{
    public class Institution
    {   // These are all the private variables associated with the Institution class.
        private string institutionName;
        private string institutionAddress;

        // InstitutionName property is used to get and set the institutionName value.
        public string InstitutionName
        {
            // Set the value to institutionName.
            set { institutionName = value.Trim(); }
            // Returns the value of institutionName.
            get { return institutionName; }
        }

        // InstitutionAddress property is used to get and set the institutionAddress value.
        public string InstitutionAddress
        {
            // Set the value to institutionAddress.
            set { institutionAddress = value.Trim(); }
            // Returns the value of institutionAddress.
            get { return institutionAddress; }
        }
    }
}
