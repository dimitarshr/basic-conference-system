﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The Certificate class displays a window with the name and institution of the Attendee along with 
 *              the conference name and the price to be paid as determined by the getCost() method.
 * Date last modified: 23/10/2016 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _40201757___Conference_System
{
    /// <summary>
    /// The Invoice window represents an invoice. The window displays the name and institution of the Attendee along with 
    /// the conference name and the price to be paid as determined by the getCost() method.
    /// </summary>
    public partial class Invoice : Window
    {
        Attendee person;

        // The constructor of this class takes an instance of Attendee class as only one person at a time can get the invoice.
        public Invoice(Attendee refPerson)
        {
            person = refPerson;
            InitializeComponent();
        }

        // On load all the labels are populated with the relevant information.
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // All the textboxes are set to be readonly.
            txtName.IsReadOnly = true;
            txtInstitution.IsReadOnly = true;
            txtInstitutionAddress.IsReadOnly = true;
            txtConference.IsReadOnly = true;
            txtPrice.IsReadOnly = true;

            // All the labels are populated with the relevant information
            txtName.Text = person.FirstName + " " + person.SecondName;
            txtInstitution.Text = person.AcademyName;
            txtInstitutionAddress.Text = person.AcademyAddress;
            txtConference.Text = person.ConferenceName;
            txtPrice.Text = "£"+person.getCost().ToString();
        }
    }
}
