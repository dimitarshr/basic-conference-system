﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The MainWindow is the main graphical interface for the Conference System.
 * Date last modified: 23/10/2016 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _40201757___Conference_System
{
    /// <summary>
    /// The MainWindow is the main graphical interface for the Conference System.
    /// You can do different things with this system:
    ///     1. The Set button allows the registration of attendee.
    ///     2. The Clear button clears the whole form from any data.
    ///     3. The Get button restores the saved data for registered attendee.
    ///     4. The Invoice button prints an invoice for the attendee.
    ///     5. The Certificate button prints a certificate showing details of the attendee and the Conference he/she attended.
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        // A variable of type Attendee is created.
        Attendee person;

        // When the Set button is clicked this block is executed.
        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            person = new Attendee();
            int refNumber;
      
            // Try to initialise all these variables.
            try
            {
                person.FirstName = txtFirstName.Text;
                person.SecondName = txtSecondName.Text;
                person.ConferenceName = txtConferenceName.Text;
                person.RegistrationType = cbRegistrationType.Text;
            }
            // If it fails, catch and display the exception.
            catch (Exception except)
            {
                MessageBox.Show(except.Message);
                person = null;
                return;
            }

            // Try to parse the Attendee Reference number. If it contains characters, it will fail the check.
            if (int.TryParse(txtAttendeeRef.Text.Trim(), out refNumber))
            {
                // Try to initialise this variable.
                try
                {
                    person.AttendeeRef = refNumber;
                }
                // If it fails, catch and display the exception.
                catch (Exception except)
                {
                    MessageBox.Show(except.Message);
                    person = null;
                    return;
                }
            }
            // If the Reference number contains characters this error message is displayed.
            else
            {
                MessageBox.Show("The reference value must be a number!");
                person = null;
                return;
            }

            // Initialise these variables. 
            person.AcademyName = txtInstitutionName.Text;
            person.AcademyAddress = txtInstitutionAddress.Text;            
            person.Paid = checkBoxPaid.IsChecked.Value;
            person.Presenter = checkBoxPresenter.IsChecked.Value;

            // If the person is Presenter this field is enabled.
            if (txtPaperTitle.IsEnabled)
            {
                // Try to initialise this variable.
                try
                {
                    person.PaperTitle = txtPaperTitle.Text;
                }
                // If it fails, catch and display the exception.
                catch (Exception except)
                {
                    MessageBox.Show(except.Message);
                    person = null;
                    return;
                }
            }
            // If everything goes ok, a nice message is displayed.
            MessageBox.Show("You are successfully registered.");
        }

        // On load of the windown these values are populated in the Registration Type combobox.
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbRegistrationType.Items.Add("Full");
            cbRegistrationType.Items.Add("Student");
            cbRegistrationType.Items.Add("Organiser");
        }

        // When the Clear button is clicked all the information is deleted from the form.
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtFirstName.Text = "";
            txtSecondName.Text = "";
            txtAttendeeRef.Text = "";
            txtInstitutionName.Text = "";
            txtInstitutionAddress.Text = "";
            txtConferenceName.Text = "";
            cbRegistrationType.Text = "";
            checkBoxPaid.IsChecked = false;
            checkBoxPresenter.IsChecked = false;
            lblAsterik_PaperTitle.Visibility = Visibility.Hidden;
            txtPaperTitle.Text = "";
        }

        // When the Get button is clicked the form is populated with all the existing information.
        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            // Check if there is a registered attendee.
            if (person != null)
            {
                txtAttendeeRef.Text = person.AttendeeRef.ToString();
                txtFirstName.Text = person.FirstName;
                txtSecondName.Text = person.SecondName;
                txtInstitutionName.Text = person.AcademyName;
                txtInstitutionAddress.Text = person.AcademyAddress;
                txtConferenceName.Text = person.ConferenceName;
                cbRegistrationType.Text = person.RegistrationType;
                checkBoxPaid.IsChecked = person.Paid;
                checkBoxPresenter.IsChecked = person.Presenter;
                txtPaperTitle.Text = person.PaperTitle;
            }
            // If there is not, display this error message.
            else
            {
                MessageBox.Show("First you have to specify details!");
            }
        }

        // If the Attendee is a Presenter the PaperTitle textbox is enabled and is compulsory to fill in some information in it.
        private void checkBoxPresenter_Checked(object sender, RoutedEventArgs e)
        {
            txtPaperTitle.IsEnabled = true;
            lblAsterik_PaperTitle.Visibility = Visibility.Visible;
        }

        // If the Attendee is not a Presenter the PaperTitle textbox is disabled.
        private void checkBoxPresenter_Unchecked(object sender, RoutedEventArgs e)
        {
            txtPaperTitle.IsEnabled = false;
            lblAsterik_PaperTitle.Visibility = Visibility.Hidden;
            txtPaperTitle.Text = "";
        }

        // When the Invoice button is clicked a new window form is shown.
        private void btnInvoice_Click(object sender, RoutedEventArgs e)
        {
            if (person != null)
            {
                Invoice ticket = new Invoice(person);
                ticket.ShowDialog();
            }
            else
            {
                MessageBox.Show("Register first by clicking the 'Set' button!");
            }
        }

        // When the Certificate button is clicked a new window form is shown.
        private void btnCertificate_Click(object sender, RoutedEventArgs e)
        {
            if (person != null)
            {
                Certificate certificate = new Certificate(person);
                certificate.ShowDialog();
            }
            else
            {
                MessageBox.Show("Register first by clicking the 'Set' button!");
            }

        }
    }
}
