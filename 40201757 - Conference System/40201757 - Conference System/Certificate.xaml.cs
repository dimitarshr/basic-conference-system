﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The Certificate class displays a window that represents a certificate of attendance.
 * Date last modified: 17/10/2016 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _40201757___Conference_System
{
    /// <summary>
    /// The Certificate window represents a certificate of attendance.
    /// It states "This is to certify that 'firstName' 'secondName' attended 'conferenceName'".
    /// If the Attendee is presenting it should state 
    /// "This is to certify that 'firstName' 'secondName' attended 'conferenceName' and presented a paper entitled 'paperTitle'".
    /// </summary>
    public partial class Certificate : Window
    {
        Attendee person;

        // The constructor of this class takes an instance of Attendee class as only one certificate can be printed for every person at a time.
        public Certificate(Attendee attendee)
        {
            person = attendee;
            InitializeComponent();

        }

        // On load all the labels are populated with the relevant information.
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lblName.Content = person.FirstName + " " + person.SecondName;
            // In case the person is not a presenter only the conference name value should be included.
            if (!person.Presenter)
            {
                lblConference.Content = person.ConferenceName+".";
            }

            // If the person is a presenter the paper title should be included as well.
            else
            {
                lblConference.Content = person.ConferenceName+",";
                lblPaperInfo.Visibility = Visibility.Visible;
                lblPaperTitle.Content = person.PaperTitle+".";
            }
        }
    }
}
