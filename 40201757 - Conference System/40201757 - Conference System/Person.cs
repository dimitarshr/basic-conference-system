﻿/*
 * Author name: Dimitar Hristov
 * Matriculation number: 40201757
 * Description: The Person class consists of all the associated properties. 
 *              It is inherited in the Attendee class.
 * Date last modified: 17/10/2016 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _40201757___Conference_System
{
    public class Person
    {
        // These are all the private variables associated with the Person class.
        private string firstName;
        private string secondName;

        // FirstName property is used to get, set and validate the firstName value.
        public string FirstName
        {
            set
            {
                // If the validation fails an exception is thrown.
                if (value.Trim() == "")
                {
                    throw new ArgumentException("All the fields with * must be populated!");
                }
                // Otherwise the firstName value is set.
                firstName = value.Trim();
            }
            // Returns the firstName value.
            get { return firstName; }
        }

        // SecondName property is used to get, set and validate the secondName value.
        public string SecondName
        {
            set
            {
                // If the validation fails an exception is thrown.
                if (value.Trim() == "")
                {
                    throw new ArgumentException("All the fields with * must be populated!");
                }
                // Otherwise the secondName value is set.
                secondName = value.Trim();
            }
            // Returns the secondName value.
            get { return secondName; }
        }
    }
}
